//
//  main.m
//  ClipAndShare
//
//  Created by Eric on 9/22/2014.
//  Copyright (c) Your Mom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
