//
//  AppDelegate.h
//  ClipAndShare
//
//  Created by Eric on 9/22/2014.
//  Copyright (c) Your Mom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
